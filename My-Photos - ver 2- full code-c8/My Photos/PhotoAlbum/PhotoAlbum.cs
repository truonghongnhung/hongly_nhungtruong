﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections.ObjectModel; // ho tro lop Collection.
using System.IO;

namespace PhotoAlbum
{
    public class PhotoAlbum : Collection<Photo>, IDisposable
    {
        private bool hasChanged = false;

        public bool HasChanged
        {
            get
            {
                if (hasChanged) return true;
                foreach (Photo p in this) //duyet trong Photo theo bien p kieu Photo
                    if (p.Haschanged) return true;
                return false;
            }
            set
            {
                hasChanged = value;
                if (value == false) foreach (Photo p in this) p.Haschanged = false;
            }
        }

        public void Dispose()
        {
            foreach (Photo p in this)
                p.Dispose();
        }

        public Photo Add(string fileName)
        {
            Photo p = new Photo(fileName);
            base.Add(p);
            return p;
        }

        protected override void ClearItems()//ham xoa het tat ca cac Items.
        {
            if (Count > 0)
            {
                Dispose();
                base.ClearItems();
                HasChanged = true;//cap nhat lai giao dien thong qua thuoc tinh HasChanged.
            }
        }

        protected override void InsertItem(int index, Photo item)
        {
            base.InsertItem(index, item);
            HasChanged = true;
        }

        protected override void RemoveItem(int index)
        {
            Items[index].Dispose();
            base.RemoveItem(index);
            HasChanged = true;
        }

        protected override void SetItem(int index, Photo item)
        {
            base.SetItem(index, item);
            HasChanged = true;
        }

        static public PhotoAlbum ReadAlbum(string path)
        {
            StreamReader sr = null;
            try
            {
                sr = new StreamReader(path); string version = sr.ReadLine();
                PhotoAlbum album = new PhotoAlbum(); switch (version) { case "63":          ReadAlbumV63(sr, album); break; default:          throw new AlbumStorageException("Unrecognized album version"); }
                album.HasChanged = false; return album;
            }

            catch (FileNotFoundException fnx) { throw new AlbumStorageException("Unable to read album " + path, fnx); }

            finally { if (sr != null)        sr.Close(); }
        }

        static private void ReadAlbumV63(StreamReader sr, PhotoAlbum album)
        {    // Read each photo into album.    
            Photo p;
            do
            {
                p = ReadPhotoV63(sr);
                if (p != null) album.Add(p);
            }
            while (p != null);
        }

        static private Photo ReadPhotoV63(StreamReader sr)
        {    // Presume at the start of photo    string file = sr.ReadLine();    if (file == null || file.Length == 0)      return null;
            // File not null, should find photo    Photograph p = new Photograph(file);
            p.Caption = sr.ReadLine(); p.DateTaken = DateTime.Parse(sr.ReadLine()); p.Photo = sr.ReadLine(); p.Notes = sr.ReadLine();
            return p;
        }



    }
}