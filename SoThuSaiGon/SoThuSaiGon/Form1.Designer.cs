﻿namespace SoThuSaiGon
{
    partial class frmSTSG
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.lbThuMoi = new System.Windows.Forms.Label();
            this.lbDS = new System.Windows.Forms.Label();
            this.lstThuMoi = new System.Windows.Forms.ListBox();
            this.lstDS = new System.Windows.Forms.ListBox();
            this.lbTime = new System.Windows.Forms.Label();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.hồSơToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.tảiDSToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.lưuDSToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.kếtThúcToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.sửaĐổiToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.mnvCopy = new System.Windows.Forms.ToolStripMenuItem();
            this.mnvCut = new System.Windows.Forms.ToolStripMenuItem();
            this.mnvPaste = new System.Windows.Forms.ToolStripMenuItem();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.btnSave = new System.Windows.Forms.Button();
            this.toolTip1 = new System.Windows.Forms.ToolTip(this.components);
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            this.menuStrip1.SuspendLayout();
            this.tableLayoutPanel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // lbThuMoi
            // 
            this.lbThuMoi.Location = new System.Drawing.Point(3, 0);
            this.lbThuMoi.Name = "lbThuMoi";
            this.lbThuMoi.Size = new System.Drawing.Size(100, 23);
            this.lbThuMoi.TabIndex = 0;
            this.lbThuMoi.Text = "Thú mới";
            // 
            // lbDS
            // 
            this.lbDS.Location = new System.Drawing.Point(158, 0);
            this.lbDS.Name = "lbDS";
            this.lbDS.Size = new System.Drawing.Size(100, 23);
            this.lbDS.TabIndex = 1;
            this.lbDS.Text = "Danh sách con thú";
            // 
            // lstThuMoi
            // 
            this.lstThuMoi.AllowDrop = true;
            this.lstThuMoi.FormattingEnabled = true;
            this.lstThuMoi.Items.AddRange(new object[] {
            "Khi",
            "Meo",
            "Cop",
            "Su tu",
            "Bep",
            "Ran ho",
            "Vuon"});
            this.lstThuMoi.Location = new System.Drawing.Point(3, 37);
            this.lstThuMoi.Name = "lstThuMoi";
            this.lstThuMoi.Size = new System.Drawing.Size(149, 186);
            this.lstThuMoi.TabIndex = 2;
            this.lstThuMoi.DragEnter += new System.Windows.Forms.DragEventHandler(this.ListBox_DragEnter);
            this.lstThuMoi.MouseDown += new System.Windows.Forms.MouseEventHandler(this.ListBox_MouseDown);
            // 
            // lstDS
            // 
            this.lstDS.AllowDrop = true;
            this.lstDS.FormattingEnabled = true;
            this.lstDS.Location = new System.Drawing.Point(158, 37);
            this.lstDS.Name = "lstDS";
            this.lstDS.Size = new System.Drawing.Size(141, 186);
            this.lstDS.TabIndex = 3;
            this.lstDS.DragDrop += new System.Windows.Forms.DragEventHandler(this.lstDS_DragDrop);
            this.lstDS.DragEnter += new System.Windows.Forms.DragEventHandler(this.ListBox_DragEnter);
            // 
            // lbTime
            // 
            this.tableLayoutPanel1.SetColumnSpan(this.lbTime, 2);
            this.lbTime.ImageAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.lbTime.Location = new System.Drawing.Point(3, 238);
            this.lbTime.Name = "lbTime";
            this.lbTime.Size = new System.Drawing.Size(317, 23);
            this.lbTime.TabIndex = 4;
            this.lbTime.Text = "Bây giờ 8h ngày 25 tháng 3 năm 2015";
            this.lbTime.Click += new System.EventHandler(this.lbTime_Click);
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.hồSơToolStripMenuItem,
            this.sửaĐổiToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(476, 24);
            this.menuStrip1.TabIndex = 6;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // hồSơToolStripMenuItem
            // 
            this.hồSơToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tảiDSToolStripMenuItem,
            this.lưuDSToolStripMenuItem,
            this.kếtThúcToolStripMenuItem});
            this.hồSơToolStripMenuItem.Name = "hồSơToolStripMenuItem";
            this.hồSơToolStripMenuItem.Size = new System.Drawing.Size(50, 20);
            this.hồSơToolStripMenuItem.Text = "Hồ sơ";
            // 
            // tảiDSToolStripMenuItem
            // 
            this.tảiDSToolStripMenuItem.Name = "tảiDSToolStripMenuItem";
            this.tảiDSToolStripMenuItem.Size = new System.Drawing.Size(152, 22);
            this.tảiDSToolStripMenuItem.Text = "Tải DS";
            // 
            // lưuDSToolStripMenuItem
            // 
            this.lưuDSToolStripMenuItem.Name = "lưuDSToolStripMenuItem";
            this.lưuDSToolStripMenuItem.Size = new System.Drawing.Size(152, 22);
            this.lưuDSToolStripMenuItem.Text = "Lưu DS";
            this.lưuDSToolStripMenuItem.Click += new System.EventHandler(this.Save);
            // 
            // kếtThúcToolStripMenuItem
            // 
            this.kếtThúcToolStripMenuItem.Name = "kếtThúcToolStripMenuItem";
            this.kếtThúcToolStripMenuItem.Size = new System.Drawing.Size(152, 22);
            this.kếtThúcToolStripMenuItem.Text = "Kết thúc";
            // 
            // sửaĐổiToolStripMenuItem
            // 
            this.sửaĐổiToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.mnvCopy,
            this.mnvCut,
            this.mnvPaste});
            this.sửaĐổiToolStripMenuItem.Name = "sửaĐổiToolStripMenuItem";
            this.sửaĐổiToolStripMenuItem.ShortcutKeyDisplayString = "";
            this.sửaĐổiToolStripMenuItem.Size = new System.Drawing.Size(58, 20);
            this.sửaĐổiToolStripMenuItem.Text = "Sửa đổi";
            // 
            // mnvCopy
            // 
            this.mnvCopy.Name = "mnvCopy";
            this.mnvCopy.ShortcutKeyDisplayString = "";
            this.mnvCopy.Size = new System.Drawing.Size(95, 22);
            this.mnvCopy.Text = "Sao";
            // 
            // mnvCut
            // 
            this.mnvCut.Name = "mnvCut";
            this.mnvCut.ShortcutKeyDisplayString = "";
            this.mnvCut.Size = new System.Drawing.Size(95, 22);
            this.mnvCut.Text = "Cắt";
            // 
            // mnvPaste
            // 
            this.mnvPaste.Name = "mnvPaste";
            this.mnvPaste.ShortcutKeyDisplayString = "";
            this.mnvPaste.Size = new System.Drawing.Size(95, 22);
            this.mnvPaste.Text = "Dán";
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 3;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.tableLayoutPanel1.Controls.Add(this.lstDS, 2, 1);
            this.tableLayoutPanel1.Controls.Add(this.lbThuMoi, 1, 0);
            this.tableLayoutPanel1.Controls.Add(this.lbDS, 2, 0);
            this.tableLayoutPanel1.Controls.Add(this.lbTime, 1, 2);
            this.tableLayoutPanel1.Controls.Add(this.lstThuMoi, 1, 1);
            this.tableLayoutPanel1.Location = new System.Drawing.Point(22, 27);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 3;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 14.37126F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 85.62875F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 51F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(422, 290);
            this.tableLayoutPanel1.TabIndex = 7;
            // 
            // btnSave
            // 
            this.btnSave.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.btnSave.Location = new System.Drawing.Point(341, 269);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(75, 23);
            this.btnSave.TabIndex = 8;
            this.btnSave.Text = "Lưu DS";
            this.btnSave.UseVisualStyleBackColor = true;
            this.btnSave.Click += new System.EventHandler(this.Save);
            // 
            // timer1
            // 
            this.timer1.Enabled = true;
            this.timer1.Interval = 1000;
            this.timer1.Tick += new System.EventHandler(this.timer1_Tick);
            // 
            // frmSTSG
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoSize = true;
            this.ClientSize = new System.Drawing.Size(476, 363);
            this.Controls.Add(this.btnSave);
            this.Controls.Add(this.tableLayoutPanel1);
            this.Controls.Add(this.menuStrip1);
            this.MainMenuStrip = this.menuStrip1;
            this.Name = "frmSTSG";
            this.Text = "Sở thú Sài Gòn";
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.tableLayoutPanel1.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lbThuMoi;
        private System.Windows.Forms.Label lbDS;
        private System.Windows.Forms.ListBox lstThuMoi;
        private System.Windows.Forms.ListBox lstDS;
        private System.Windows.Forms.Label lbTime;
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem hồSơToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem tảiDSToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem lưuDSToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem kếtThúcToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem sửaĐổiToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem mnvCopy;
        private System.Windows.Forms.ToolStripMenuItem mnvCut;
        private System.Windows.Forms.ToolStripMenuItem mnvPaste;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.Button btnSave;
        private System.Windows.Forms.ToolTip toolTip1;
        private System.Windows.Forms.Timer timer1;
    }
}

