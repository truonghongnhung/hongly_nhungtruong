﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Calculator
{
    public partial class frmCalculator : Form
    {
        private bool isTypingNumber = false;

        enum PhepToan { Cong, Tru, Nhan, Chia, PhanSo, PhanTram, Can, CongTru, Backspace };
        PhepToan pheptoan;
        double nho=0;


        public frmCalculator()
        {
            InitializeComponent();
        }

        private void btnTru_Click(object sender, EventArgs e)
        {
        }

        private void frmCalculator_Load(object sender, EventArgs e)
        {

        }

        private void btn0_Click(object sender, EventArgs e)
        {

        }

        private void btn1_Click(object sender, EventArgs e)
        {

        }

        private void btn2_Click(object sender, EventArgs e)
        {

        }

        private void btn3_Click(object sender, EventArgs e)
        {

        }

        private void btn4_Click(object sender, EventArgs e)
        {

        }

        private void btn5_Click(object sender, EventArgs e)
        {

        }

        private void btn6_Click(object sender, EventArgs e)
        {

        }

        private void btn7_Click(object sender, EventArgs e)
        {

        }

        private void btn8_Click(object sender, EventArgs e)
        {

        }

        private void btn9_Click(object sender, EventArgs e)
        {

        }

        private void btnCong_Click(object sender, EventArgs e)
        {            
        }

        private void btnNhan_Click(object sender, EventArgs e)
        {
        }

        private void btnChia_Click(object sender, EventArgs e)
        {           
        }

        private void btnBang_Click(object sender, EventArgs e)
        {

            isTypingNumber = false;
            double ketqua=0.0;

            switch (pheptoan)
            {
                case PhepToan.Cong:  ketqua = nho + double.Parse(label1.Text); break;
                case PhepToan.Tru: { ketqua = (double.Parse(label1.Text) - nho) * (-1); } break;
                case PhepToan.Nhan: ketqua = nho * double.Parse(label1.Text); break;
                case PhepToan.Chia: ketqua = nho / double.Parse(label1.Text); break;
                case PhepToan.PhanSo: ketqua = 1 / double.Parse(label1.Text); break;
                case PhepToan.PhanTram: ketqua = double.Parse(label1.Text) / 100; break;
                case PhepToan.Can: ketqua = Math.Sqrt( double.Parse(label1.Text)); break;
                case PhepToan.CongTru: ketqua = nho+(-1)*double.Parse(label1.Text); break;
                case PhepToan.Backspace:
                    {
                       ketqua = nho/10+double.Parse(label1.Text) ;
                       nho = ketqua;
                    }
                    break;
            }
            
            label1.Text = ketqua.ToString();
            
        }

        private void btnTru_Click_1(object sender, EventArgs e)
        {
        }

        private void btnC_Click(object sender, EventArgs e)
        {
            isTypingNumber = false;
            label1.Text = "0.";
            nho = 0;
        }

        private void NhapSo(object sender, EventArgs e)
        {
            if (isTypingNumber)
                label1.Text = label1.Text + ((Button)sender).Text;
            else
            {
                label1.Text = ((Button)sender).Text;
                isTypingNumber = true;
            }
        }

        private void NhapPhepToan(object sender, EventArgs e)
        {
            isTypingNumber = false;

            switch(((Button)sender).Text)
            {
                case "+":pheptoan=PhepToan.Cong;break;
                case "-":pheptoan=PhepToan.Tru;break;
                case "*":pheptoan=PhepToan.Nhan;break;
                case "/":pheptoan=PhepToan.Chia;break;
                case "1/x": pheptoan = PhepToan.PhanSo; break;
                case "%": pheptoan = PhepToan.PhanTram; break;
                case "can": pheptoan = PhepToan.Can; break;
                case "+/-": pheptoan = PhepToan.CongTru; break;
                case "Backspace": pheptoan = PhepToan.Backspace; break;

            }
            //??? lập trình các nút còn lại
            //xử lý sự kiện bàn phím( xử lý chung qua form, chọn keypress để cho ra hàm nhấn các phím trên bàn phím: ... đọc tài liệu, sử dụng lệnh switch ).vd,btn1.Forcus()
            // viền khung các nút có màu xanh, khi bấm Tab sẽ tự chuyển nút. tab:đi tiếp. Shift Tab:đi ngc lại, forcus:có viền xanh
            //tab order: chỉnh lại theo thứ tự từ đầu đến cuối để dùng tab order 
            
            TinhKetQua();

            nho = double.Parse(label1.Text);
        }

        private void TinhKetQua()
        {
            double ketqua = 0.0;
            switch (pheptoan)
            {
                case PhepToan.Cong: ketqua = double.Parse(label1.Text); break;
                case PhepToan.Tru: ketqua = (double.Parse(label1.Text)); break;
                case PhepToan.Nhan: ketqua = double.Parse(label1.Text); break;
                case PhepToan.Chia: ketqua =  double.Parse(label1.Text); break;
                case PhepToan.PhanSo: ketqua =double.Parse(label1.Text); break;
                case PhepToan.PhanTram: ketqua =  double.Parse(label1.Text); break;
                case PhepToan.Can: ketqua = double.Parse(label1.Text); break;
                case PhepToan.CongTru: ketqua = (-1)*double.Parse(label1.Text); break;
                case PhepToan.Backspace: ketqua = (int)double.Parse(label1.Text) / 10;   break;
            
            }
            label1.Text = ketqua.ToString();

        }

        private void button1_Click(object sender, EventArgs e)
        {

        }

        private void btnphay_Click(object sender, EventArgs e)
        {
            label1.Text = label1.Text + "." ;
        }
    }
}
