﻿namespace Calculator
{
    partial class frmCalculator
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.btn1 = new System.Windows.Forms.Button();
            this.btn2 = new System.Windows.Forms.Button();
            this.btn3 = new System.Windows.Forms.Button();
            this.btn4 = new System.Windows.Forms.Button();
            this.btn5 = new System.Windows.Forms.Button();
            this.btn6 = new System.Windows.Forms.Button();
            this.btn7 = new System.Windows.Forms.Button();
            this.btn8 = new System.Windows.Forms.Button();
            this.btn9 = new System.Windows.Forms.Button();
            this.btn0 = new System.Windows.Forms.Button();
            this.btnphay = new System.Windows.Forms.Button();
            this.btnCong = new System.Windows.Forms.Button();
            this.btnNhan = new System.Windows.Forms.Button();
            this.btnChia = new System.Windows.Forms.Button();
            this.btnTru = new System.Windows.Forms.Button();
            this.btnBang = new System.Windows.Forms.Button();
            this.btnC = new System.Windows.Forms.Button();
            this.btnCan = new System.Windows.Forms.Button();
            this.btn1x = new System.Windows.Forms.Button();
            this.btnphantram = new System.Windows.Forms.Button();
            this.btnCongTru = new System.Windows.Forms.Button();
            this.btnBackspace = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.BackColor = System.Drawing.SystemColors.GradientActiveCaption;
            this.label1.Location = new System.Drawing.Point(28, 19);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(232, 37);
            this.label1.TabIndex = 0;
            this.label1.Text = "0.";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // btn1
            // 
            this.btn1.Location = new System.Drawing.Point(12, 225);
            this.btn1.Name = "btn1";
            this.btn1.Size = new System.Drawing.Size(30, 30);
            this.btn1.TabIndex = 15;
            this.btn1.Text = "1";
            this.btn1.UseVisualStyleBackColor = true;
            this.btn1.Click += new System.EventHandler(this.NhapSo);
            // 
            // btn2
            // 
            this.btn2.Location = new System.Drawing.Point(67, 225);
            this.btn2.Name = "btn2";
            this.btn2.Size = new System.Drawing.Size(30, 30);
            this.btn2.TabIndex = 16;
            this.btn2.Text = "2";
            this.btn2.UseVisualStyleBackColor = true;
            this.btn2.Click += new System.EventHandler(this.NhapSo);
            // 
            // btn3
            // 
            this.btn3.Location = new System.Drawing.Point(128, 225);
            this.btn3.Name = "btn3";
            this.btn3.Size = new System.Drawing.Size(30, 30);
            this.btn3.TabIndex = 17;
            this.btn3.Text = "3";
            this.btn3.UseVisualStyleBackColor = true;
            this.btn3.Click += new System.EventHandler(this.NhapSo);
            // 
            // btn4
            // 
            this.btn4.Location = new System.Drawing.Point(12, 179);
            this.btn4.Name = "btn4";
            this.btn4.Size = new System.Drawing.Size(30, 30);
            this.btn4.TabIndex = 10;
            this.btn4.Text = "4";
            this.btn4.UseVisualStyleBackColor = true;
            this.btn4.Click += new System.EventHandler(this.NhapSo);
            // 
            // btn5
            // 
            this.btn5.Location = new System.Drawing.Point(67, 179);
            this.btn5.Name = "btn5";
            this.btn5.Size = new System.Drawing.Size(30, 30);
            this.btn5.TabIndex = 11;
            this.btn5.Text = "5";
            this.btn5.UseVisualStyleBackColor = true;
            this.btn5.Click += new System.EventHandler(this.NhapSo);
            // 
            // btn6
            // 
            this.btn6.Location = new System.Drawing.Point(128, 179);
            this.btn6.Name = "btn6";
            this.btn6.Size = new System.Drawing.Size(30, 30);
            this.btn6.TabIndex = 12;
            this.btn6.Text = "6";
            this.btn6.UseVisualStyleBackColor = true;
            this.btn6.Click += new System.EventHandler(this.NhapSo);
            // 
            // btn7
            // 
            this.btn7.Location = new System.Drawing.Point(12, 133);
            this.btn7.Name = "btn7";
            this.btn7.Size = new System.Drawing.Size(30, 30);
            this.btn7.TabIndex = 5;
            this.btn7.Text = "7";
            this.btn7.UseVisualStyleBackColor = true;
            this.btn7.Click += new System.EventHandler(this.NhapSo);
            // 
            // btn8
            // 
            this.btn8.Location = new System.Drawing.Point(67, 133);
            this.btn8.Name = "btn8";
            this.btn8.Size = new System.Drawing.Size(30, 30);
            this.btn8.TabIndex = 6;
            this.btn8.Text = "8";
            this.btn8.UseVisualStyleBackColor = true;
            this.btn8.Click += new System.EventHandler(this.NhapSo);
            // 
            // btn9
            // 
            this.btn9.Location = new System.Drawing.Point(128, 133);
            this.btn9.Name = "btn9";
            this.btn9.Size = new System.Drawing.Size(30, 30);
            this.btn9.TabIndex = 7;
            this.btn9.Text = "9";
            this.btn9.UseVisualStyleBackColor = true;
            this.btn9.Click += new System.EventHandler(this.NhapSo);
            // 
            // btn0
            // 
            this.btn0.Location = new System.Drawing.Point(12, 270);
            this.btn0.Name = "btn0";
            this.btn0.Size = new System.Drawing.Size(85, 30);
            this.btn0.TabIndex = 20;
            this.btn0.Text = "0";
            this.btn0.UseVisualStyleBackColor = true;
            this.btn0.Click += new System.EventHandler(this.NhapSo);
            // 
            // btnphay
            // 
            this.btnphay.Location = new System.Drawing.Point(128, 270);
            this.btnphay.Name = "btnphay";
            this.btnphay.Size = new System.Drawing.Size(30, 30);
            this.btnphay.TabIndex = 21;
            this.btnphay.Text = ".";
            this.btnphay.UseVisualStyleBackColor = true;
            this.btnphay.Click += new System.EventHandler(this.btnphay_Click);
            // 
            // btnCong
            // 
            this.btnCong.Location = new System.Drawing.Point(179, 270);
            this.btnCong.Name = "btnCong";
            this.btnCong.Size = new System.Drawing.Size(30, 30);
            this.btnCong.TabIndex = 22;
            this.btnCong.Text = "+";
            this.btnCong.UseVisualStyleBackColor = true;
            this.btnCong.Click += new System.EventHandler(this.NhapPhepToan);
            // 
            // btnNhan
            // 
            this.btnNhan.Location = new System.Drawing.Point(179, 179);
            this.btnNhan.Name = "btnNhan";
            this.btnNhan.Size = new System.Drawing.Size(30, 30);
            this.btnNhan.TabIndex = 13;
            this.btnNhan.Text = "*";
            this.btnNhan.UseVisualStyleBackColor = true;
            this.btnNhan.Click += new System.EventHandler(this.NhapPhepToan);
            // 
            // btnChia
            // 
            this.btnChia.Location = new System.Drawing.Point(179, 133);
            this.btnChia.Name = "btnChia";
            this.btnChia.Size = new System.Drawing.Size(30, 30);
            this.btnChia.TabIndex = 8;
            this.btnChia.Text = "/";
            this.btnChia.UseVisualStyleBackColor = true;
            this.btnChia.Click += new System.EventHandler(this.NhapPhepToan);
            // 
            // btnTru
            // 
            this.btnTru.Location = new System.Drawing.Point(179, 225);
            this.btnTru.Name = "btnTru";
            this.btnTru.Size = new System.Drawing.Size(30, 30);
            this.btnTru.TabIndex = 18;
            this.btnTru.Text = "-";
            this.btnTru.UseVisualStyleBackColor = true;
            this.btnTru.Click += new System.EventHandler(this.NhapPhepToan);
            // 
            // btnBang
            // 
            this.btnBang.Location = new System.Drawing.Point(230, 225);
            this.btnBang.Name = "btnBang";
            this.btnBang.Size = new System.Drawing.Size(30, 75);
            this.btnBang.TabIndex = 19;
            this.btnBang.Text = "=";
            this.btnBang.UseVisualStyleBackColor = true;
            this.btnBang.Click += new System.EventHandler(this.btnBang_Click);
            // 
            // btnC
            // 
            this.btnC.Location = new System.Drawing.Point(128, 97);
            this.btnC.Name = "btnC";
            this.btnC.Size = new System.Drawing.Size(30, 30);
            this.btnC.TabIndex = 2;
            this.btnC.Text = "C";
            this.btnC.UseVisualStyleBackColor = true;
            this.btnC.Click += new System.EventHandler(this.btnC_Click);
            // 
            // btnCan
            // 
            this.btnCan.Location = new System.Drawing.Point(230, 101);
            this.btnCan.Name = "btnCan";
            this.btnCan.Size = new System.Drawing.Size(42, 23);
            this.btnCan.TabIndex = 4;
            this.btnCan.Text = "can";
            this.btnCan.UseVisualStyleBackColor = true;
            this.btnCan.Click += new System.EventHandler(this.NhapPhepToan);
            // 
            // btn1x
            // 
            this.btn1x.Location = new System.Drawing.Point(227, 179);
            this.btn1x.Name = "btn1x";
            this.btn1x.Size = new System.Drawing.Size(33, 30);
            this.btn1x.TabIndex = 14;
            this.btn1x.Text = "1/x";
            this.btn1x.UseVisualStyleBackColor = true;
            this.btn1x.Click += new System.EventHandler(this.NhapPhepToan);
            // 
            // btnphantram
            // 
            this.btnphantram.Location = new System.Drawing.Point(230, 139);
            this.btnphantram.Name = "btnphantram";
            this.btnphantram.Size = new System.Drawing.Size(30, 23);
            this.btnphantram.TabIndex = 9;
            this.btnphantram.Text = "%";
            this.btnphantram.UseVisualStyleBackColor = true;
            this.btnphantram.Click += new System.EventHandler(this.NhapPhepToan);
            // 
            // btnCongTru
            // 
            this.btnCongTru.Location = new System.Drawing.Point(179, 97);
            this.btnCongTru.Name = "btnCongTru";
            this.btnCongTru.Size = new System.Drawing.Size(30, 30);
            this.btnCongTru.TabIndex = 3;
            this.btnCongTru.Text = "+/-";
            this.btnCongTru.UseVisualStyleBackColor = true;
            this.btnCongTru.Click += new System.EventHandler(this.NhapPhepToan);
            // 
            // btnBackspace
            // 
            this.btnBackspace.Location = new System.Drawing.Point(12, 97);
            this.btnBackspace.Name = "btnBackspace";
            this.btnBackspace.Size = new System.Drawing.Size(85, 30);
            this.btnBackspace.TabIndex = 1;
            this.btnBackspace.Text = "Backspace";
            this.btnBackspace.UseVisualStyleBackColor = true;
            this.btnBackspace.Click += new System.EventHandler(this.NhapPhepToan);
            // 
            // frmCalculator
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(284, 312);
            this.Controls.Add(this.btnBackspace);
            this.Controls.Add(this.btnCongTru);
            this.Controls.Add(this.btnphantram);
            this.Controls.Add(this.btn1x);
            this.Controls.Add(this.btnCan);
            this.Controls.Add(this.btnC);
            this.Controls.Add(this.btnBang);
            this.Controls.Add(this.btnTru);
            this.Controls.Add(this.btnChia);
            this.Controls.Add(this.btnNhan);
            this.Controls.Add(this.btnCong);
            this.Controls.Add(this.btnphay);
            this.Controls.Add(this.btn0);
            this.Controls.Add(this.btn9);
            this.Controls.Add(this.btn8);
            this.Controls.Add(this.btn7);
            this.Controls.Add(this.btn6);
            this.Controls.Add(this.btn5);
            this.Controls.Add(this.btn4);
            this.Controls.Add(this.btn3);
            this.Controls.Add(this.btn2);
            this.Controls.Add(this.btn1);
            this.Controls.Add(this.label1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.Name = "frmCalculator";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Calculator";
            this.Load += new System.EventHandler(this.frmCalculator_Load);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button btn1;
        private System.Windows.Forms.Button btn2;
        private System.Windows.Forms.Button btn3;
        private System.Windows.Forms.Button btn4;
        private System.Windows.Forms.Button btn5;
        private System.Windows.Forms.Button btn6;
        private System.Windows.Forms.Button btn7;
        private System.Windows.Forms.Button btn8;
        private System.Windows.Forms.Button btn9;
        private System.Windows.Forms.Button btn0;
        private System.Windows.Forms.Button btnphay;
        private System.Windows.Forms.Button btnCong;
        private System.Windows.Forms.Button btnNhan;
        private System.Windows.Forms.Button btnChia;
        private System.Windows.Forms.Button btnTru;
        private System.Windows.Forms.Button btnBang;
        private System.Windows.Forms.Button btnC;
        private System.Windows.Forms.Button btnCan;
        private System.Windows.Forms.Button btn1x;
        private System.Windows.Forms.Button btnphantram;
        private System.Windows.Forms.Button btnCongTru;
        private System.Windows.Forms.Button btnBackspace;
    }
}

