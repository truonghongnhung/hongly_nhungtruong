﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;

namespace SoThuSaiGon
{
    public partial class frmSTSG : Form
    {
        public frmSTSG()
        {
            InitializeComponent();
        }

        private void ListBox_MouseDown(object sender, MouseEventArgs e)
        {
            ListBox lb = (ListBox)sender;
            int index = lb.IndexFromPoint(e.X, e.Y);
            lb.DoDragDrop(lb.Items[index].ToString(), DragDropEffects.Copy);            
        }

        private void ListBox_DragEnter(object sender, DragEventArgs e)
        {
            if (e.Data.GetDataPresent(DataFormats.Text)) e.Effect = DragDropEffects.Copy;
            else e.Effect = DragDropEffects.Move;
        }

        private void lstDS_DragDrop(object sender, DragEventArgs e)
        {
            //kiemt tra du lieu sao chep co dung k
            if (e.Data.GetDataPresent(DataFormats.StringFormat))
            {
                ListBox lb = (ListBox)sender;

                lb.Items.Add(e.Data.GetData(DataFormats.Text));
            }
         
        }

        private void Save(object sender, EventArgs e)
        {
            StreamWriter writer = new StreamWriter("danhsachsothu.txt");
            if (writer == null) return;

            foreach (var item in lstDS.Items)
                writer.WriteLine(item.ToString());

            writer.Close();
        }

        private void tảiDSToolStripMenuItem_Click()
        {
            
            StreamReader reader = new StreamReader("thumoi.txt");
            if (reader == null) return;

            string input = null;
            while ((input = reader.ReadLine()) != null)
            {
                lstThuMoi.Items.Add(input);
            }
            reader.Close();

            using (StreamReader rs = new StreamReader("danhsahcthu.txt"))
            {
                input = null;
                while ((input = rs.ReadLine()) != null)
                {
                    lstThuMoi.Items.Add(input);
                }
            }

        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            lbTime.Text = string.Format("Bay gio la {0}:{1}:{2} ngay {3} thang {4} nam {5}", DateTime.Now.Hour, DateTime.Now.Minute,DateTime.Now.Second, DateTime.Now.Day, DateTime.Now.Month, DateTime.Now.Year); ;
        }

        private void lbTime_Click(object sender, EventArgs e)
        {

        }

       
    }
}
