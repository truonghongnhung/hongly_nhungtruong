﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace PhoTooTool
{
    public partial class Photos : Form
    {
        public Photos()
        {
            InitializeComponent();
        }

        private void mnuLoad_Click(object sender, EventArgs e)
        {
            OpenFileDialog dlg = new OpenFileDialog();
            dlg.Title = "Open photo";
            dlg.Filter = "jpg files (*.jpg)|*.jpg|All file(*.*)|*.*";//phan *.jpg dau la hien th len form, phan sau la 

            if (dlg.ShowDialog() == DialogResult.OK)
            {
                try
                {
                    pbxPhoto.Image = new Bitmap(dlg.OpenFile());
                    SetStatusStrip(dlg.FileName);
                }
                catch (ArgumentException ex)
                {
                    MessageBox.Show("Unable to load files " + ex.Message);
                    pbxPhoto.Image = null;
                }
            }
            dlg.Dispose();
        }

        private void ProcessPhoto(object sender, ToolStripItemClickedEventArgs e)
        {
            ToolStripItem item = e.ClickedItem;
            string enumVal = item.Tag as string;
            if (enumVal != null) pbxPhoto.SizeMode = (PictureBoxSizeMode)Enum.Parse(typeof(PictureBoxSizeMode), enumVal);
        }

        private void mnuImage_DropDownOpening(object sender, EventArgs e)  //thay doi vi tri dau tick o menu View\image
        {
            ToolStripDropDownItem parent = (ToolStripDropDownItem)sender;
            if (parent != null)
            {
                string enumVal = pbxPhoto.SizeMode.ToString();//.ToString() : dua ra dang chuoi cac gia tri cua 1 dtuong nao do
                foreach (ToolStripMenuItem item in parent.DropDownItems)
                {
                    item.Enabled = (pbxPhoto.Image != null);
                    item.Checked = item.Tag.Equals(enumVal); //so sanh 2 chuoi = phep Equals,ko dung duoc phep == de so sanh 2 chuoi.
                }
            }
        }

        private void SetStatusStrip(string path)//duong dan den tap tin, gan duong dan do vao ham setstt nay 
        {
            if (pbxPhoto.Image != null)
            {
                sttInfo.Text = path;//gan cho sttInfor = duong dan
                sttImageSize.Text = string.Format("{0:#} x {1:#}", pbxPhoto.Image.Width, pbxPhoto.Image.Height);
                //sttAlbumPos.Text=
            }
            else
            {
                sttInfo = null;
                sttImageSize = null;
                sttAlbumPos = null;
            }
        }
    }
}
