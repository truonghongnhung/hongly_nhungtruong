﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;

namespace PhotoAlbum
{
    class AlbumStorage
    {
        public class AlbumStorageException : Exception
        {
            public AlbumStorageException() : base() { }
            public AlbumStorageException(string msg) : base(msg) { }
            public AlbumStorageException(string msg, Exception inner) : base(msg, inner) { }

            public static class AlbumStorage
            {
                static private int CurrentVersion = 63;
                static public void WriteAlbum(PhotoAlbum album, string path)
                {
                    StreamWriter sw = null;
                    try
                    {
                        sw = new StreamWriter(path, false);
                        sw.WriteLine(CurrentVersion.ToString());
                        // Store each photo separately        
                        foreach (Photograph p in album) WritePhoto(sw, p);
                        // Reset changed after all photos written      
                        album.HasChanged = false;
                    }
                    catch (UnauthorizedAccessException uax)
                    {
                        throw new AlbumStorageException("Unable to access album " + path, uax);
                    }
                    finally { if (sw != null)          sw.Close(); }
                }                
            }

            static private void WritePhoto(StreamWriter sw, Photo p)
            {
                sw.WriteLine(p.FileName);
                sw.WriteLine(p.Caption != null ? p.Caption : ""); sw.WriteLine(p.DateTaken.ToString()); sw.WriteLine(p.Photo != null ? p.Photo : ""); sw.WriteLine(p.Notes != null ? p.Notes : "");
            }



        }
    }
}
